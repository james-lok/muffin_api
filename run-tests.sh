# Builds a docker container for our API
sudo docker stop muffin_website
sudo docker rm muffin_website
sudo docker build -t muffin_api . 
sudo docker run -dit --name muffin_website -p 8080:80 muffin_api
# Tests the container with unit tests
python3 muffins-unit-tests.py
# Stops container
sudo docker stop muffin_website
if [ $? = 0 ]; then
    printf "[TEST] - executable built: ${EXEC}\n"
    sudo docker stop muffin_website
else
    printf "[TEST] - failed\n"
    sudo docker stop muffin_website
    exit 1 # or exit $? if you want the specific code
fi
