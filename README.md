# Welcome to the muffin project
## Prerequisites 
### 1. You have cloned this repository using the command:
```
git clone git@bitbucket.org:james-lok/muffin_api.git
```
### 2. You have set up a virtual environment for this project
### 3. The following machines are running:
* james-jenkins-muffin-project
* james-jenkins-agent-muffin-project
### 4. Have docker + docker compose installed

# Setting up the virtual environment
python3 -m venv ./.venv
source .venv/bin/activate

pip3 install -r requirements.txt

# Build project locally
make sure you are located in the root of this repo
docker-compose up

you can see the page on 

# Build project globally
check if the project is already running on:
http://jamesmuffinapi.academy.labs.automationlogic.com/

If you want to rebuild the project you can visit the jenkins server on:
http://jamesmuffinapijenkins.academy.labs.automationlogic.com:8080/
and start the job `Muffin Api CD`, wait for the build to finish and view it on the projects website stated above.

# Workflow
A developer must be working on a story to add to the project When working on this project, progress should occur in the following manner: