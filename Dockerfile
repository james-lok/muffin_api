From python:3

COPY . /usr/src/app

RUN pip3 install --no-cache-dir -r /usr/src/app/requirements.txt

Expose 80

CMD ["python", "/usr/src/app/backend/app.py"]