import unittest
import requests

class TestBakingFunctions(unittest.TestCase):     
    # Tests users only can access correct endpoints.
    def test_muffins_endpoint_successs(self):
        res = requests.get('http://127.0.0.1:8080/', timeout=5)
        self.assertEqual( 200, res.status_code )

    def test_muffin_endpoint_failure(self):
        res = requests.get('http://127.0.0.1:8080/muffins', timeout=5)
        self.assertEqual( 404, res.status_code )

    def test_prices_endpoint_successs(self):
        res = requests.get('http://127.0.0.1:8080/prices', timeout=5)
        self.assertEqual( 200, res.status_code )

    def test_price_endpoint_failure(self):
        res = requests.get('http://127.0.0.1:8080/price', timeout=5)
        self.assertEqual( 404, res.status_code )
    
    def test_products_endpoint_successs(self):
        res = requests.get('http://127.0.0.1:8081/products', timeout=5)
        self.assertEqual( 200, res.status_code )

    def test_product_endpoint_failure(self):
        res = requests.get('http://127.0.0.1:8080/product', timeout=5)
        self.assertEqual( 404, res.status_code )
    

if __name__ == '__main__':
    unittest.main()